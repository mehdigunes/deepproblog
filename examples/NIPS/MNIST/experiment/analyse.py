from pdb import set_trace
import numpy as np
from scipy import stats
import matplotlib.pyplot as plt

f = open("long-run", "r")
f1 = []
nn = []
acc = []
loss = []
for x in f:
    if x.startswith("F1"):
        f1.append(float(x.split()[1]))
    if x.startswith("NN"):
        nn.append(float(x.split()[1]))
    if x.startswith("acc"):
        acc.append(float(x.split()[1]))
    if x.startswith("Iteration"):
        if float(x.split()[1]) % 300 == 0:
            loss.append(float(x.split()[-1]))

g = open("base-long-run", "r")
f1_base = []
nn_base = []
acc_base = []
loss_base = []
for x in g:
    if x.startswith("F1"):
        f1_base.append(float(x.split()[1]))
    if x.startswith("NN"):
        nn_base.append(float(x.split()[1]))
    if x.startswith("Iteration"):
        if float(x.split()[1]) % 300 == 0:
            loss_base.append(float(x.split()[-1]))
    
# set_trace()

h = open("bad-long-run", "r")
f1_bad = []
nn_bad = []
acc_bad = []
loss_bad = []
for x in h:
    if x.startswith("F1"):
        f1_bad.append(float(x.split()[1]))
    if x.startswith("NN"):
        nn_bad.append(float(x.split()[1]))
    if x.startswith("acc"):
        acc_bad.append(float(x.split()[1]))
    if x.startswith("Iteration"):
        if float(x.split()[1]) % 300 == 0:
            loss_bad.append(float(x.split()[-1]))


t, p = stats.ttest_ind(acc_bad, nn_base)
set_trace()

x = range(0, 29000, 300)
# x2 = range(0, 29000, 100)

plt.plot(x, acc, 'r-', label="DPL")
plt.plot(x, nn, 'r--', label="CNN of DPL")
# plt.plot(x, acc_bad, 'y-', label="Bad DPL")
# plt.plot(x, nn_bad, 'y--', label="CNN of bad DPL")
plt.plot(x, nn_base, 'b--', label="CNN")
plt.xlabel('Iteration')
plt.ylabel('Accuracy')
plt.legend()
# plt.axis([0, 6, 0, 20])
plt.show()

# set_trace()

plt.plot(x[:-1], loss, 'r-')
plt.plot(x[:-1], loss_bad, 'y-')
plt.plot(x[:-1], loss_base, 'b-')
plt.xlabel('Iteration')
plt.ylabel('Average loss per 100 iterations')
# plt.axis([0, 6, 0, 20])
plt.show()

# set_trace()

# 1.0::check(0).
# 0.00001::check(1).
# 0.00001::check(2).
# 0.00001::check(3).
# 0.00001::check(4).
# 0.00001::check(5).
# 0.00001::check(6).
# 0.00001::check(7).
# 0.00001::check(8).
# 0.00001::check(9).
