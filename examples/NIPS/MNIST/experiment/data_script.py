import torch
import torchvision
import torchvision.transforms as transforms
import torch.nn as nn
from torch.autograd import Variable
import numpy as np
import os
path = os.path.abspath(__file__)
dir_path = os.path.dirname(path)

import matplotlib.pyplot as plt
import pickle
import random
from pdb import set_trace

transform = transforms.Compose([transforms.ToTensor(), transforms.Normalize((0.5,), (0.5,))])
mnist_train_data = torchvision.datasets.MNIST(root=dir_path+'/../../../data/MNIST', train=True, download=True,transform=transform)
mnist_test_data = torchvision.datasets.MNIST(root=dir_path+'/../../../data/MNIST', train=False, download=True,transform=transform)

def get_dict():
    d_train = {}
    for t in mnist_train_data:
        im = t[0]
        digit = t[1]
        if digit not in d_train.keys():
            d_train[digit] = []
        d_train[digit].append(im)

    d_test = {}
    for t in mnist_test_data:
        im = t[0]
        digit = t[1]
        if digit not in d_test.keys():
            d_test[digit] = []
        d_test[digit].append(im)


    with open('train_examples.pickle', 'wb') as f:
        pickle.dump(d_train, f)

    with open('test_examples.pickle', 'wb') as f:
        pickle.dump(d_test, f)

def make_distribution():
    d_train = pickle.load(open("train_examples.pickle", "rb" ))
    d_test = pickle.load(open("test_examples.pickle", "rb" ))

    # train
    train = [[] for i in range(10)]
    for k in range(10):
        limit = 5842 if k % 2 == 0 else 1947
        values = d_train[k]
        random.shuffle(values)
        train[k] = [(v, k) for v in values[:limit]]

    # test
    test = [[] for i in range(10)]
    for k in range(10):
        limit = 958 if k % 2 == 0 else 319
        values = d_test[k]
        random.shuffle(values)
        test[k] = [(v, k) for v in values[:limit]]

    with open('train_dataset.pickle', 'wb') as f:
        pickle.dump(train, f)

    with open('test_dataset.pickle', 'wb') as f:
        pickle.dump(test, f)

def make_queries():
    train = pickle.load(open("train_dataset.pickle", "rb" ))
    test = pickle.load(open("test_dataset.pickle", "rb" ))

    all_train = [i for k in range(10) for i in train[k]]
    all_test = [i for k in range(10) for i in test[k]]
    random.shuffle(all_train)
    random.shuffle(all_test)
    
    idx_train = [[] for i in range(10)]
    for i, example in enumerate(all_train):
        idx_train[example[1]].append(i)
    for i in idx_train:
        random.shuffle(i)

    idx_test = [[] for i in range(10)]
    for i, example in enumerate(all_test):
        idx_test[example[1]].append(i)
    for i in idx_test:
        random.shuffle(i)
    
    queries_train = []
    for i in range(10):
        limit = 4500 if i % 2 == 0 else 1500

        for j in idx_train[i][:limit]:
            q = "classify(" + str(j) + "," + str(i) + ").\n"
            queries_train.append(q)
    random.shuffle(queries_train)

    queries_test = []
    for i in range(10):
        limit = 750 if i % 2 == 0 else 250

        for j in idx_test[i][:limit]:
            q = "classify(" + str(j) + "," + str(i) + ").\n"
            queries_test.append(q)
    random.shuffle(queries_test)

    with open('trainset.pickle', 'wb') as f:
        pickle.dump(all_train, f)
    with open('testset.pickle', 'wb') as f:
        pickle.dump(all_test, f)

    with open('queries_train.pickle', 'wb') as f:
        pickle.dump(queries_train, f)
    with open('queries_test.pickle', 'wb') as f:
        pickle.dump(queries_test, f)

def print_queries():
    queries_train = pickle.load(open("queries_train.pickle", "rb" ))
    queries_test = pickle.load(open("queries_test.pickle", "rb" ))

    with open('train_data.txt','w') as f:
        for q in queries_train:
            f.write(q)


    with open('test_data.txt','w') as f:
        for q in queries_test:
            f.write(q)

    # f = open(,'w')
    # for q in queries_train:
    #     f.write(q + '\n')
    # f.close()

    # f = open('test_data.txt','w')
    # for q in queries_test:
    #     f.write(q + '\n')
    # f.close()

make_queries()
print_queries()