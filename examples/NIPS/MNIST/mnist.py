# import torch
# import torchvision
# import torchvision.transforms as transforms
# import torch.nn as nn
# from torch.autograd import Variable
# import numpy as np
# import os
# path = os.path.abspath(__file__)
# dir_path = os.path.dirname(path)

# class MNIST_Net(nn.Module):
#     def __init__(self, N=10):
#         super(MNIST_Net, self).__init__()
#         self.encoder = nn.Sequential(
#             nn.Conv2d(1,  6, 5),
#             nn.MaxPool2d(2, 2), # 6 24 24 -> 6 12 12
#             nn.ReLU(True),
#             nn.Conv2d(6, 16, 5), # 6 12 12 -> 16 8 8
#             nn.MaxPool2d(2, 2), # 16 8 8 -> 16 4 4
#             nn.ReLU(True)
#         )
#         self.classifier =  nn.Sequential(
#             nn.Linear(16 * 4 * 4, 120),
#             nn.ReLU(),
#             nn.Linear(120, 84),
#             nn.ReLU(),
#             nn.Linear(84, N),
#             nn.Softmax(1)
#         )

#     def forward(self, x):
#         x = self.encoder(x)
#         x = x.view(-1, 16 * 4 * 4)
#         x = self.classifier(x)
#         return x



# def test_MNIST(model,max_digit=10,name='mnist_net'):
#     confusion = np.zeros((max_digit,max_digit),dtype=np.uint32) # First index actual, second index predicted
#     N = 0
#     for d,l in mnist_test_data:
#         if l < max_digit:
#             N += 1
#             d = Variable(d.unsqueeze(0))
#             outputs = model.networks[name].net.forward(d)
#             _, out = torch.max(outputs.data, 1)
#             c = int(out.squeeze())
#             confusion[l,c] += 1
#     print(confusion)
#     F1 = 0
#     for nr in range(max_digit):
#         TP = confusion[nr,nr]
#         FP = sum(confusion[:,nr])-TP
#         FN = sum(confusion[nr,:])-TP
#         F1 += 2*TP/(2*TP+FP+FN)*(FN+TP)/N
#     print('F1: ',F1)
#     return [('F1',F1)]


# def neural_predicate(network, i, dataset='train'):
#     i = int(i)
#     dataset = str(dataset)
#     if dataset == 'train':
#         d, l = mnist_train_data[i]
#     elif dataset == 'test':
#         d, l = mnist_test_data[i]
#     d = Variable(d.unsqueeze(0))
#     output = network.net(d)
#     return output.squeeze(0)

# transform = transforms.Compose([transforms.ToTensor(), transforms.Normalize((0.5,), (0.5,))])
# mnist_train_data = torchvision.datasets.MNIST(root=dir_path+'/../../../data/MNIST', train=True, download=True,transform=transform)
# mnist_test_data = torchvision.datasets.MNIST(root=dir_path+'/../../../data/MNIST', train=False, download=True,transform=transform)


import torch
import torchvision
import torchvision.transforms as transforms
import torch.nn as nn
from torch.autograd import Variable
import numpy as np
import os
path = os.path.abspath(__file__)
dir_path = os.path.dirname(path)

import matplotlib.pyplot as plt
import pickle
from pdb import set_trace
from random import shuffle
from data_loader import load

class MNIST_Net(nn.Module):
    def __init__(self, N=10):
        super(MNIST_Net, self).__init__()
        self.encoder = nn.Sequential(
            nn.Conv2d(1,  6, 5),
            nn.MaxPool2d(2, 2), # 6 24 24 -> 6 12 12
            nn.ReLU(True),
            nn.Conv2d(6, 16, 5), # 6 12 12 -> 16 8 8
            nn.MaxPool2d(2, 2), # 16 8 8 -> 16 4 4
            nn.ReLU(True)
        )
        self.classifier =  nn.Sequential(
            nn.Linear(16 * 4 * 4, 120),
            nn.ReLU(),
            nn.Linear(120, 84),
            nn.ReLU(),
            nn.Linear(84, N),
            nn.Softmax(1)
        )

    def forward(self, x):
        x = self.encoder(x)
        x = x.view(-1, 16 * 4 * 4)
        x = self.classifier(x)
        return x


def test_MNIST(model,max_digit=10,name='mnist_net', iter=0, data=None): # Add possible arguments to pass variables required for loss, also call this function each time for the train (or each 5 times)
    confusion = np.zeros((max_digit,max_digit),dtype=np.uint32) # First index actual, second index predicted
    N = 0
    # Have to adjust this dataset for testing!

    # shuffle(queries_test)
    # set_trace()
    ids = [int(str(q)[9:].split(',')[0]) for q in data]
    # set_trace()
    # for d,l in mnist_test_data[:1000]: # Just make list of d and l you like!
    for idx in ids:
        d, l = mnist_train_data[idx]
        # print('d', d)
        # print('l', l)
        # exit()
        if l < max_digit:
            N += 1
            d = Variable(d.unsqueeze(0))
            outputs = model.networks[name].net.forward(d)
            _, out = torch.max(outputs.data, 1)
            c = int(out.squeeze())
            confusion[l,c] += 1

        # a = neural_predicate(model.networks[name], idx, dataset='train')
        # set_trace()
    # print(confusion, iter) # Useless (nou)
    F1 = 0
    total_TP = 0
    for nr in range(max_digit):
        TP = confusion[nr,nr]
        FP = sum(confusion[:,nr])-TP
        FN = sum(confusion[nr,:])-TP
        F1 += 2*TP/(2*TP+FP+FN)*(FN+TP)/N
        total_TP += TP
    print('F1: ', F1, iter) # Just the model performance?
    print('NN: ', total_TP/N, iter)
    acc = model.accuracy(data, test=False, iter = iter)[0][1]
    print('acc: ', acc, iter) # complete pipeline performance?
    
    # set_trace()
    return [('F1',F1)]


def neural_predicate(network, i, dataset='train'):
    i = int(i)
    dataset = str(dataset)
    # Also have to change
    if dataset == 'train':
        d, l = mnist_train_data[i]
    elif dataset == 'test':
        d, l = mnist_test_data[i]
    d = Variable(d.unsqueeze(0))
    output = network.net(d)
    return output.squeeze(0)

transform = transforms.Compose([transforms.ToTensor(), transforms.Normalize((0.5,), (0.5,))])

queries_train = load('train_data.txt')
queries_test = load('test_data.txt')
mnist_train_data = pickle.load(open("trainset.pickle", "rb" ))
mnist_test_data = pickle.load(open("testset.pickle", "rb" ))

# set_trace()

# 0.15::check(0).
# 0.05::check(1).
# 0.15::check(2).
# 0.05::check(3).
# 0.15::check(4).
# 0.05::check(5).
# 0.15::check(6).
# 0.05::check(7).
# 0.15::check(8).
# 0.05::check(9).